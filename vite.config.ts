import { defineConfig } from "vite";

export default defineConfig({
    base: '',
    root: ".",
    publicDir: "public",
    build: {
        rollupOptions: {
            input: {
                main: "index.html",
                "svg": "Test_SVG.html"
            }
        }
    }
})