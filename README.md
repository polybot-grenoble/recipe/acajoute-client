# Client Acajoute

## Installation

Le client se base sur [Vite](https://vitejs.dev/) qui permet de rafraîchir automatiquement
l'application lors de la modification d'un fichier la composant. De plus Vite permet
d'utiliser certains paquets npm compatible avec le navigateur.

Pour installer les dépendances de développement, il vous faut [Node.JS](https://nodejs.org/fr) (version > 18), puis effectuez la commande `npm i` pour installer les paquets nécessaires.

## Développement 

Pour lancer l'application, effectuez la commande `npm run test` ouvrez le lien affiché
dans votre terminal.

## Documentation

Pour générer la documentation, utilsez la commande `npm run gen_docs`, vous trouverez ensuite une 
documentation interactive au format HTML dans le dossier `docs`.