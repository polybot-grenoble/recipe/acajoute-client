type id = string;

type TypeFeuille = 
    'racine' | 
    'retour' | 
    'fonction' | 
    'variable' | 
    'assignation' | 
    'caster' | 
    'operateur' | 
    'relais' | 
    'event' | 
    'flux' | 
    'undefined';

type FlowType = 
    'normal' | 
    'retour' |
    'racine' | 
    'if' | 
    'while' | 
    'foreach' | 
    'switch';

/* === Types génériques === */

type __Argument = {
    _typeObjet : '__Argument'; 
    type: string;
    nom: string;
    precedent?: id; /* lien vers le precedent */
    defaut?: string;
};

type __Flux = {
    type: FlowType;
    in: { nom?: string, connexion?: id }[],
    out: { nom?: string, connexion?: id }[],
};

type __Feuille = {
    
    type: TypeFeuille;      // Type de feuille
    adresse: id;             // Identifiant unique
    ref: id;            // Référencement pour l'éditeur / le traducteur sur l'identité de la feuille
    flux?: __Flux;
    args?: {                // __Arguments de la feuille
        in?: __Argument[];    // __Arguments d'entrée
        out?: __Argument[];   // Valeurs de retour
    };

};

type __Arbre = {
    
    adresse: id;
    nom: string;
    racine: id;

    inputArgs: __Argument[];
    outputArgs: __Argument[];
    
    feuilles: { 
        [key: id]: __Feuille;
    };

}

type __Projet = {

    adresse: id;
    nom: string;
    var_globales: { [key: id]: __Argument };
    fonctions: { [key: id]: __Arbre };

}