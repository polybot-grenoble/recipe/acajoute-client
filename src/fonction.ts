import { CacheFonction } from "./projet";

export class Fonction {

    public onchange: (cache: CacheFonction) => void = () => {};
    public onrequestUpdate: () => CacheFonction | undefined;

    constructor (private _cache: CacheFonction) {

        this.onrequestUpdate = () => undefined;

        this.regenRacine();
        this.regenReturns();

    }

    get id () {
        return this._cache.adresse;
    }

    get nom () {
        return this._cache.nom;
    }

    get feuilles () {
        return this._cache.feuilles;
    }

    get racine () {
        return this._cache.racine;
    }

    /**
     * Retourne les arguments d'entrée de la fonction
     */
    get inputArgs (): __Argument[] {
        return this._cache.inputArgs;
    }

    /**
     * Retourne les arguments de sortie de la fonction
     */
    get outputArgs (): __Argument[] {
        return this._cache.outputArgs;
    }

    /**
     * Met à jour la feulle racine avec les bons arguments de sortie
     */
    regenRacine () {
        let r = this._cache.feuilles[this._cache.racine];
        r.args = {
            out: this._cache.inputArgs            
        }
        this._cache.feuilles[this._cache.racine] = r;
        this.onchange(this._cache);
    }

    /**
     * Met à jour toutes les feuilles retour avec les bons arguments de sortie
     */
    regenReturns () {

        for (let _f in this._cache.feuilles) {

            let F = this._cache.feuilles[_f];
            if (F.type != "retour") continue;

            F.args = {
                in: this._cache.outputArgs
            }
            this._cache.feuilles[_f] = F;

        }

    }

    /**
     * Met à jour le cache en demandant à la page d'édition liée par le callback `onrequestUpdate`
     */
    updateCache () {

        let cache = this.onrequestUpdate();
        // if (!cache) throw "Function " + this.id + " not linked to an editor !";
        if (!cache) return;

        this._cache = cache;

    }

    export (): CacheFonction {
        
        this.updateCache();
        return this._cache;

    }

    /**
     * Cache d'une fonction vide
     */
    static vide (adresse: string, nom: string): CacheFonction {

        let ret: CacheFonction = {
            adresse, nom,
            racine: `${adresse}:racine_0`,
            inputArgs: [],
            outputArgs: [],
            feuilles: { }
        }

        ret.feuilles[`${adresse}:racine_0`] = {
            __ui: { x: 0.5, y: 0.25, h: 1, w: 1 },
            type: "racine",
            ref: "#:def",
            adresse: `${adresse}:racine_0`,
            args: {
                out: []
            },
            flux: { 
                type: "racine",
                out: [ { connexion: `${adresse}:retour_0:in:0` } ],
                in: [],
            }
        };
        
        ret.feuilles[`${adresse}:retour_0`] = {
            __ui: { x: 2.5, y: 0.5, h: 1, w: 1 },
            type: "retour",
            ref: "#:return",
            adresse: `${adresse}:retour_0`,
            args: {
                in: []
            },
            flux: { 
                type: "retour",
                in: [ { connexion: `${adresse}:racine_0:out:0` } ],
                out: []
            }
        };

        return ret;

    }

}