import { Fonction } from "./fonction";
import { Client, NetManager } from "./net/net";
import { INVALID_TYPE, Type } from "./ui/bulles/type";
import { PageManager } from "./ui/pages";
import { PageFonction } from "./ui/pages/fonction";
import { grosseBoite } from "./ui/panneau/boite";
import { boiteBulle } from "./ui/panneau/boitebulle";

export type BoitesProjet = {
    fonctions: grosseBoite,
    vars: grosseBoite,
    libs: grosseBoite,
}

export type FonctionWrapper = {
    page?: PageFonction,
    boite: boiteBulle,
    fonction: Fonction,
}

export type RefBulle = {
    ref: string,
    flux_entree: boolean,
    couleur: string,
    nom: string,
}

export class Projet {

    private _cache: CacheProjet;

    private _fonctions: { [key: string]: FonctionWrapper } = {};

    private _types: Type[] = [];

    private _remoteProjet: RemoteProjet;
    private _listenerId: number;

    private _references: RefBulle[] = [];

    constructor (private _pageManager: PageManager, private server: NetManager, private _boiteManager: BoitesProjet, cache?: CacheProjet | { remote_id: string }) {
        this._listenerId = this.server.socket.on((p)=>{
            if(!p.command) return;
            switch(p.command){
                case 'project_init':
                case 'project_load':
                    if(p.args && p.args[0]){
                        this._remoteProjet = <RemoteProjet>JSON.parse(p.args[0]);
                        this._cache.nom = this._remoteProjet.name;
                        this._cache.id = this._remoteProjet.id;
                    }
                    if(p.args && p.args[1]){
                        let _importProjet = <CacheProjet>JSON.parse(p.args[1]);
                        this._cache.fonctions = _importProjet.fonctions;
                        this._cache.var_globales = _importProjet.var_globales;
                    }
                    this._updateUI();
                    console.log(`Projet ${this._cache.nom} chargé !`);

                    (<HTMLDivElement>document.querySelector("#current-projet")).innerHTML = this._cache.nom;
                    break;
                case 'project_save':
                    alert(`Projet ${this._cache.nom} sauvegardé !`);
                    break;
            }
        })


        if (!cache) {
            this._cache = Projet.vide();
            this.server.socket.send('/projects', 'init', [(<Client>this.server.client).id, this._cache.nom, 'local']);
        } else if (cache.remote_id != undefined) {
            this._cache = Projet.vide();
            this.server.socket.send('/projects', 'load', [cache.remote_id]);
        } else if (cache.remote_id) {
            this._cache = <CacheProjet>cache;
            if((<CacheProjet>cache).id=="init"){
                this.server.socket.send('/projects', 'init', [(<Client>this.server.client).id, this._cache.nom, 'local']);
                return;
            }
            this._updateUI();
        }
    }

    get id () {
        return this._cache.id;
    }

    get nom () {
        return this._cache.nom;
    }

    /**
     * Met à jour le cache selon les éditeurs ouverts
     */
    updateCache () {

        for (let _id in this._fonctions) {
            this._cache.fonctions[_id] = this._fonctions[_id].fonction.export();
        }

    }

    /**
     * Demande le projet au serveur
     */
    sync (): void {
        
    }
    
    /**
     * Permet de sauvegarder le projet
     */
    sauvegarde (commit: string): void {
        this.sauvegardeWeb();
        this.server.socket.send('/projects', 'save', [(<Client>this.server.client).id, this._remoteProjet.id, JSON.stringify(this.export(), undefined, 4), commit]);
    }

    /**
     * Permet de sauvegarder localement le projet
     */
    sauvegardeWeb (): void {
        localStorage.setItem("remote_id", this._remoteProjet.id);
        localStorage.setItem("save", JSON.stringify(this.export()));
    }

    /**
     * Exporte le projet 
     */
    export (): CacheProjet {

        this.updateCache();
        return this._cache;

    }
    /**
     * Ferme le projet.
     */
    ferme (): void {
        this.server.socket.removeListener(this._listenerId);
    }

    /**
     * Retourne le type associé à un id de type
    */
    type (type_id: string): Type {

        for (let t of this._types) {
            if (t.id == type_id) return t;
        }

        return INVALID_TYPE;

    }

    ref (ref_id: string): RefBulle | undefined {
        return this._references.filter(e => e.ref == ref_id)[0];
    }

    /**
     * Charge des types
     * @param types Les types à charger
     */
    loadTypes (types: Type[]) {
        this._types = [ ...this._types, ...types ];
    }

    /**
     * Ajoute des références de bulles
     * @param refs Références à ajouter
     */
    loadRefs (refs: RefBulle[]) {
        this._references = [ ...this._references, ...refs ];
    }

    /**
     * Permet d'importer une fonction
     * @param fn Le cache de la fonction à importer
     */
    importFn (fn: CacheFonction) {

        let [ _, id ] = fn.adresse.split(":");
        if (!id) id = "__imported";
        fn.adresse = `${this.id}:${id}`;
        this._cache.fonctions[id] = fn;

        this._updateUI();

    }

    /**
     * Supprime l'ensemble de l'affichage
     */
    private _clearUI() {

        for (let _fn in this._fonctions) {
            let w = this._fonctions[_fn];
            if (w.page) this._pageManager.closePage(w.page.id);
        }

        this._boiteManager.fonctions.clear();

    }

    /**
     * Met à jour l'affichage selon le cache
     */
    private _updateUI () {

        this._clearUI()

        for (let _fn in this._cache.fonctions) {

            let fonction = new Fonction(this._cache.fonctions[_fn]);

            let boite = this._boiteManager.fonctions.creerBulle('simple', fonction.nom, "var(--theme)");
            boite.self.ondblclick = () => {
                let wrap = this._fonctions[_fn];
                if (wrap.page) return;
                wrap.page = new PageFonction(wrap.fonction, this, this._pageManager);
                wrap.page.onclose = () => {
                    wrap.page = undefined;
                }
            }

            let wrap: FonctionWrapper = {
                boite, fonction 
            }

            this._fonctions[_fn] = wrap;
            
        }

    }

    /**
     * Crée un cache de projet vide
     */
    static vide (_id?: string, _nom?: string): CacheProjet {
        let id = _id || "new_project";
        let nom = _nom || "Nouveau Projet";

        let _projet: CacheProjet = {
            id,
            nom,
            fonctions: {},
            var_globales: {},
            remote_id: undefined,
        };

        let main = Fonction.vide(id + ":main", 'main');

        _projet.fonctions["main"] = main;

        return _projet;

    }

}

export type CacheProjet = {

    remote_id: undefined,
    id: string;
    nom: string;
    var_globales: { [key: string]: __Argument };
    fonctions: { [key: string]: CacheFonction };

};

export type RemoteProjet = {
    id: string;
    name: string;
    clientId: string;
    createdOn: number;
    lastAccessed: number;
    origin: origin;
    repoId?: number;
}

export type origin = "local" | "gitlab";

export type CacheFonction = {

    adresse: string;
    nom: string;
    racine: string;

    inputArgs: __Argument[];
    outputArgs: __Argument[];

    feuilles: { 
        [key: string]: CacheFeuille;
    };

}

export type CacheFeuille = __Feuille & { 
    __ui: {
        x: number,
        y: number,
        w: number,
        h: number,
    }
};
