import { Fonction } from "./fonction";
import { CacheFeuille, Projet } from "./projet";
import { PageManager } from "./ui/pages";
import { PageFonction } from "./ui/pages/fonction";
import { PageProjet } from "./ui/pages/projet";
import { grosseBoite } from "./ui/panneau/boite";
import { Identity, NetManager } from "./net/net";

window.onload = () => {
    let identity = new Identity();
    let netManager = new NetManager("ws://localhost:8080/socket", identity.id);

    let projetCourant: Projet;

    netManager.socket.on((p)=>{
        console.log(`Received packet ${p.command} ${p.args}`);
    })

    //netManager.socket.send("/projects", "list");

    hookBodyCtrls();
    demoRuban();

    let __fenetre = <HTMLDivElement>document.body.querySelector(".fenetre");
    let __onglets = <HTMLDivElement>__fenetre.querySelector(".onglets");
    let __pages = <HTMLDivElement>__fenetre.querySelector(".pages");
    
    let __wrapPanneau = <HTMLDivElement> document.querySelector("body > div.panneau > div");
    let boites = {
        fonctions: new grosseBoite(1, "Fonctions", "square-root-alt"),
        vars: new grosseBoite(2, "Variables globales", "globe"),
        libs: new grosseBoite(3, "Librairies", "book")
    }
    __wrapPanneau.append(boites.fonctions.self, boites.vars.self, boites.libs.self);

    boites.fonctions.opened = true;

    let pages = new PageManager(__pages, __onglets);

    netManager.onReady((p)=>{
        let id = <string>localStorage['remote_id'];
        if(id){
            projetCourant = new Projet(pages, netManager, boites, {remote_id: id});
        }else{
            projetCourant = new Projet(pages, netManager, boites);
        }

        projetCourant.loadTypes([
            { id: "std:bool", casts: [], couleur: "red", nom: "Booléen", variante: "simple" },
            { id: "std:greg", casts: [], couleur: "#f000f0", nom: "Greg", variante: "simple" }
        ]);

        projetCourant.loadRefs([
            { couleur: "#8e44ad", flux_entree: false, nom: "Définition", ref: "#:def" },
            { couleur: "#c0392b", flux_entree: true, nom: "Si", ref: "#:if" },
            { couleur: "#8e44ad", flux_entree: true, nom: "Retour", ref: "#:return" },
        ])

        projetCourant.importFn(testFunctionCache());

        (<HTMLInputElement>document.querySelector("#save_projet")).onclick = () => {
            if(!projetCourant) return;
            let commit: string | null;
            do {
                commit = prompt("Commit : ");
            }while(!commit);
            
            projetCourant.sauvegarde(commit);
        }

        (<HTMLInputElement>document.querySelector("#new_projet")).onclick = () => {
            let nomCont = (<HTMLInputElement>document.querySelector("#new_projet_title"));
            let nom: string = "Projet sans titre";

            if(nomCont.value) nom = nomCont.value;
            console.log(nom)
            
            if(projetCourant){               
                if(!confirm("Les données non sauvegardées seront perdues.")) return;
                projetCourant.ferme();
                projetCourant = new Projet(pages, netManager, boites, Projet.vide("init", nom));
            }
        }



        /*
        setInterval(()=>{projet.sauvegardeWeb()}, 5000);

        setInterval(()=>{projet.sauvegarde("commit")}, 10000);
*/
        //@ts-ignore
        window.projet = projetCourant;
            
        // let test = localStorage["testFn"] ? JSON.parse(localStorage["testFn"]) : testFunctionCache();

        // let f = new Fonction(test);
        // // setInterval(() => localStorage["testFn"] = JSON.stringify(f.export()), 1000);

        // let pageFonction = new PageFonction(f, projet, pages);
        
        // //@ts-ignore
        // window.page = pageFonction;
        // //@ts-ignore
        // window.fn = f;
    })

};

function hookBodyCtrls () {
    let air = <HTMLInputElement>document.querySelector("#ctrls-air");
    let dark = <HTMLInputElement>document.querySelector("#ctrls-dark");
    let theme = <HTMLSelectElement>document.querySelector("#ctrls-theme");

    air.onchange = () => {
        document.body.classList.toggle("air", air.checked);
    }

    dark.onchange = () => {
        document.body.classList.toggle("dark", dark.checked);
        document.body.classList.toggle("light", !dark.checked);
    }

    theme.onchange = () => {
        document.body.classList.remove("theme-bleu");
        document.body.classList.remove("theme-rose");
        document.body.classList.remove("theme-vert");
        document.body.classList.remove("theme-grenoble");
        document.body.classList.add(`theme-${theme.value}`);
    }

    dark.checked = window.matchMedia("(prefers-color-scheme: dark)").matches;

}

function demoRuban () {

    let bandeau = <HTMLDivElement>document.querySelector(".bandeau");
    let ruban = <HTMLDivElement>bandeau.querySelector(".ruban");
    let elems = <NodeListOf<HTMLDivElement>>ruban.querySelectorAll(".elem");

    document.querySelectorAll(".boite").forEach((el)=>{el.setAttribute("hidden", "true");});
    document.querySelectorAll(`#boite_Fichier`).forEach((el)=>{el.removeAttribute("hidden");});
    let i = 0;
    
    for (let el of elems) {
        el.onclick = () => {
            let box = el.getBoundingClientRect();
            let rubox = ruban.getBoundingClientRect();
            ruban.style.setProperty("--slider-width", box.width + "px");
            ruban.style.setProperty("--slider-pos", box.x - rubox.x + "px");
            let title = el.querySelector("label")?.innerText;
            document.querySelectorAll(".boite").forEach((el)=>{el.setAttribute("hidden", "true");});
            document.querySelectorAll(`#boite_${title}`).forEach((el)=>{el.removeAttribute("hidden");});
        }
    }

    ruban.onwheel = e => {
        e.preventDefault();
        console.log(e.deltaY)
        i = Math.min(3, Math.max(0, e.deltaY > 0 ? i + 1 : i - 1));
        let el = elems[i];
        let box = el.getBoundingClientRect();
        let rubox = ruban.getBoundingClientRect();
        ruban.style.setProperty("--slider-width", box.width + "px");
        ruban.style.setProperty("--slider-pos", box.x - rubox.x + "px");
    }

    ruban.ondblclick = () => {
        bandeau.classList.toggle("compact")
    }
}

function testFunctionCache () {
    
    let test = Fonction.vide("zero:test", "Test");

    test.feuilles["zero:test:if_0"] = <CacheFeuille>{
        adresse: "zero:test:if_0",
        args: {
            in: [
                { nom: "Condition", type: "std:bool", defaut: "True" }
            ],
            out: [
                { nom: "Ouais c'est Greg", type: "std:greg" },
                { nom: "boule", type: "std:bool" }
            ]
        },
        flux: {
            type: "if",
            in: [ { connexion: "zero:test:racine_0:out:0" } ],
            out: [ 
                { nom: "Vrai" },
                { nom: "Faux" /*, connexion: "zero:test:retour_0:in:0"*/ },
                { connexion: "zero:test:retour_0:in:0" } 
            ],
        },
        ref: "#:if",
        type: "flux",
        __ui: {
            x: 1.5, y: 1.25, w: 0, h: 0
        }
    };
    
    (<__Flux>test.feuilles["zero:test:racine_0"].flux).out[0] = { connexion: "zero:test:if_0:in:0" };
    (<__Flux>test.feuilles["zero:test:retour_0"].flux).in[0] = { connexion: "zero:test:if_0:out:2" };
    test.feuilles["zero:test:retour_0"].__ui.x = 4.5;
    (<__Feuille["args"] & {}>test.feuilles["zero:test:retour_0"].args).in = [ 
        { nom: "Greg", type: "std:greg", defaut: "Ouais", _typeObjet: "__Argument" },
        { nom: "La vérité", type: "std:bool", defaut: "False", _typeObjet: "__Argument", precedent: "zero:test:if_0:out:1" },
        { nom: "Greg", type: "std:greg", defaut: "Ouais", _typeObjet: "__Argument" },
    ]

    test.feuilles["zero:test:retour_1"] = JSON.parse(JSON.stringify(test.feuilles["zero:test:retour_0"]));
    test.feuilles["zero:test:retour_1"].__ui = { y: 3.5, x: 4.5, h: 1, w: 1 };
    //@ts-ignore
    test.feuilles["zero:test:retour_1"].flux.in[0].connexion = "";


    test.inputArgs = [
        { nom: "x", type: "std:bool", _typeObjet: "__Argument" }
    ]

    test.outputArgs = [
        { nom: "Greg", type: "std:greg", _typeObjet: "__Argument", defaut: "Ouais" }
    ]

    return test;

}