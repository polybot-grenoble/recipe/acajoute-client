import {v4 as uuidv4} from 'uuid'
import { Command, EWSClient, callbackPacket, packet } from './ews';

export class NetManager {

    socket: EWSClient;
    client?: Client;
    origin?: origin;
    private accCont: HTMLDivElement;
    private nomCont: HTMLSpanElement;
    private onReadyCb?: callbackPacket;

    constructor(url: string, clientId: string){

        this.socket = new EWSClient(url, clientId);

        this.socket.on((p)=>this.processPacket(p))

        this.socket.send("/clients", "client_local_login", [clientId]);

        this.accCont = <HTMLDivElement>document.querySelector(".account");
        this.nomCont = <HTMLSpanElement>this.accCont.querySelector(".info span");

        this.nomCont.innerText = "Chargement...";
        this.accCont.title = 'Chargement...';
        
        this.accCont.onclick = () => {
            if(!this.client) return;
            if(this.origin == 'local'){
                window.location.assign(`http://localhost:8080/oauth2/gitlab?clientId=${this.client.id}`);
            }else{
                window.location.assign(`http://localhost:8080/oauth2/gitlab/logout?clientId=${this.client.id}`);
            }
        }
        
    }

    onReady(cb: callbackPacket): void{
        this.onReadyCb = cb;
    }

    private processPacket(p: packet){
        if(!p.command) return;
            switch(p.command){
                case "client_local_login":
                    if(p.args && p.args[0] && p.args[1]){
                        this.client = <Client>JSON.parse(p.args[0]);
                        if(this.client.name){
                            this.nomCont.innerText = this.client.name;
                        }
                        
                        this.accCont.title = "Se connecter à Gitlab"
                        this.origin = <origin>p.args[1];

                        if(this.origin == 'gitlab'){
                            this.accCont.classList.toggle("connecte",true);
                            this.accCont.title = "Se déconnecter de Gitlab"
                        }

                        if(this.onReadyCb) this.onReadyCb(p);
                    }

                break;
                case "error":
                    if(p.args && p.args[0]){
                        console.error("BACKEND ERROR: ", p.args[0]);
                    }
                break;
            }
    }

}

export class Identity {

    id: string;

    constructor(){
        let temp = localStorage.getItem("acajoute_client_id");
        
        if(!temp){
            this.id = uuidv4();
            localStorage.setItem("acajoute_client_id", this.id);
        }else{
            this.id = temp;
        }
    }
}

export type Client = {
    id: string;
    name?: string;
    last_login: number;
    created_on: number;
}

export type origin = 'gitlab' | 'local';