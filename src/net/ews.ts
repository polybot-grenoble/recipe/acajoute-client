import {v4 as uuidv4} from 'uuid'

export class EWSClient {

    private id: string;
    private ws: WebSocket;
    private queue: packet[];
    private client_id?: string;
    private listeners: Map<number,callbackPacket>;
    private ready: boolean;
    private counter: number;

    constructor(url: string, client_id?: string){
        this.ws = new WebSocket(url);
        this.queue = [];
        this.listeners = new Map();
        this.client_id = client_id;
        this.ready = false;
        this.counter = 0;

        this.ws.onerror = (event) => {
            console.error(`Error from EWS: ${event.type}`);
        };

        this.ws.onopen = (event) => {
            console.log("EWS connected.");
            setInterval(()=>{this.flushQueue()}, 500);
        }

        this.ws.onmessage = (message)=>{    
            let p: packet = JSON.parse(message.data);
            if(p.command==Command.CLIENT_INIT_ID && !this.id && p.args){
                this.id = p.args[0];
                this.ready = true;
                return;
            }
            for(let listener of this.listeners.values()){
                listener(p);
            }
        }
    }

    send(path: string, command: string, args?: string[]): void {
        let header: packetHeader = {packetId: uuidv4(), socketId: this.id, clientId: this.client_id, path: path, serverPath: path};
        let p: packet = {command: command, args: args, header: header};

        if(this.ws.readyState != this.ws.OPEN || !this.ready){
            console.log("Packet in queue. Waiting for connection to be ready to send.")
            this.queue.push(p);
            return;
        }

        this.ws.send(JSON.stringify(p));
    }

    on(cb: callbackPacket): number {
        this.listeners.set(this.counter++, cb);
        return this.counter;
    }

    removeListener(id: number): boolean {
        return this.listeners.delete(id);
    }

    disconnect(): void {
        this.ws.close();
    }

    flushQueue(): void {
        if(this.queue.length != 0 && this.ws.readyState == this.ws.OPEN && this.ready){
            console.log("Flushing packet queue.");
            for(let p of this.queue){
                if(!p.header.socketId) p.header.socketId = this.id;
                this.ws.send(JSON.stringify(p));
            }
            this.queue = []
        }
    }

}
export type callbackPacket = (packet: packet) => void;

export type packetHeader = {
    packetId: string;
    clientId?: string;
    socketId: string;
    path: string;
    serverPath: string;
}

export type packet = {
    header: packetHeader;
    command?: string;
    args?: string[];
};

export const enum Command {
    CLIENT_INIT_ID="client_init_id"
}