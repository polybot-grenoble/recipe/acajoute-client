import { BarreOnglets, HTMLOnglet, addOnglet, closeOnglet, getOngletValue, initBarreOnglets, selectOnglet } from "./onglets"

export type PageCallback = (ctrl: PageManager) => void;
const DEF_CALLBACK = () => {};

export interface Page {
    id: string,
    nom: string,
    elem: HTMLDivElement,
    onglet: HTMLOnglet,
    onload: PageCallback,
    onclose: PageCallback,
};
export class PageManager {

    protected manager: {
    
        elem: HTMLDivElement,
        onglets: BarreOnglets,
        pages: Page[],
    
        __etat: {
            preventCloseEvent: boolean,
            preventChangeEvent: boolean,
        },
    
    };

    /**
     * Initialise le gesionnaire de pages
     * @param pageCont Element contenant les pages
     * @param ongletCont Element contenant les onglets
     * @returns Le gestionnaire de pages
     */
    constructor (pageCont: HTMLDivElement, ongletCont: HTMLDivElement) {

        let onglets = initBarreOnglets(ongletCont);

        this.manager = {
            elem: pageCont, onglets, pages: [],
            __etat: {
                preventCloseEvent: false,
                preventChangeEvent: false,
            }
        };
    
        onglets.onchange = id => {
            if (this.manager.__etat.preventChangeEvent) {
                this.manager.__etat.preventChangeEvent = false;
                return;
            }
            this.selectPage(id);
        };
    
        onglets.onclose = id => {
            if (this.manager.__etat.preventCloseEvent) {
                this.manager.__etat.preventCloseEvent = false;
                return;
            }
            this.closePage(id);
        }

    }
    
    /**
     * Ajoute une page et l'affiche
     * @param id L'id de la page
     * @param nom Le nom de la page
     * @returns la page
     */
    addPage (id: string, nom: string) {
        console.log(`%c[Page Manager]%c Ouverture de la page ${id}`, "color: #0482ff", "color: inherit");
    
        // On ajoute l'onglet 
        let onglet: HTMLOnglet = addOnglet(this.manager.onglets, nom, id);

        // On construit la page
        let page: Page = {
            elem: document.createElement("div"),
            id, nom, onclose: DEF_CALLBACK, onload: DEF_CALLBACK,
            onglet
        };

        // On configure l'élément
        page.elem.className = "page";
        page.elem.id = `__page_${id}`;
        page.elem.dataset.nom = nom; // <- A titre purement indicatif dans l'inspecteur d'élément

        // On ajoute la page et on l'active
        this.manager.pages.push(page);
        this.selectPage(id);
        
        return page;
    }

    
    /**
     * Affiche une page
     * @param id L'id de la page
     */
    selectPage (id: string): void {

        console.log(`%c[Page Manager]%c Selection de la page ${id || '<vide>'}`, "color: #0482ff", "color: inherit");
    
        let page: Page | undefined = this.getPage(id);
        if (!page) {
            console.warn(`%c[Page Manager]%c La page ${id || '<vide>'} n'existe pas`, "color: #0482ff", "color: inherit");
            return;
        }
    
        // On enlève la page déjà affichée (même si on sait pas qui c'est)
        this.manager.pages.forEach(e => e.elem.remove());
        
        // On affiche la nouvelle
        this.manager.elem.appendChild(page.elem);    
    
        // On active son onglet correspondant
        this.manager.__etat.preventChangeEvent = true; // On empêche une boucle infinie
        selectOnglet(this.manager.onglets, id);
    
    }
        
    /**
     * Ferme une page
     * @param manager Le gestionnaire de pages
     * @param id L'id de la page
     */
    closePage (id: string): void {

        console.log(`%c[Page Manager]%c Fermeture de la page ${id}`, "color: #0482ff", "color: inherit");
        
        // On récupère la page
        let page = this.getPage(id);
        if (!page) return;
    
        // On la supprime
        this.manager.pages = this.manager.pages.filter(e => e != page);
        page.elem.remove();
    
        // On supprime l'onglet correspondant
        closeOnglet(this.manager.onglets, page.onglet);
            // + Comme la selection d'onglet va changer, la nouvelle page sera affichée automatiquement !!!
    
        page.onclose(this);

        // Si il n'y a plus de pages, on fait RIEN
        if (!this.manager.pages.length) return;
    
    }

    /**
     * Récupère une page selon son id
     * @param manager Le gestionnaire de pages
     * @param id L'id de la page
     * @returns La page si elle existe, `undefined` sinon
     */
    getPage (id: string): Page | undefined {
        return this.manager.pages.filter(e => e.id == id)[0];
    }

}