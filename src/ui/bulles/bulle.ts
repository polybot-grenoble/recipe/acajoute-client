import { CacheFeuille } from "../../projet";
import { Argument } from "./argument";
import { Type } from "./type";

export interface Bulle {
    id: string,

    x: number,
    y: number,
    height: number,
    width: number,

    main_color: string,
    sec_color: string,

    feuille: __Feuille,
    self: HTMLDivElement,
}

type __flux = { 
    connexion?: string, 
    elem: HTMLDivElement, 
    box: HTMLDivElement, 
    txt: HTMLSpanElement 
};

export class BulleBase implements Bulle {

    private _x: number;
    private _y: number;
    private _w: number;
    private _h: number;

    private _colors: string[] = [ "var(--bg)", "var(--bg)" ];
    private elem: HTMLDivElement;
    
    private _titre: HTMLDivElement;

    private _cont: HTMLDivElement;
    private _cont_in: HTMLDivElement;
    private _cont_out: HTMLDivElement;

    protected _type: TypeFeuille = "undefined";
    protected _mode_flux: FlowType = "normal";

    private _flux_entree: { 
        connexion?: string, 
        elem: HTMLDivElement, 
        box: HTMLDivElement, 
        txt: HTMLSpanElement 
    } & { existe: boolean };

    private _flux_sortie: __flux[] = [];

    protected _arguments: {
        in: Argument[],
        out: Argument[]
    } = { in: [], out: [] };

    constructor (private _id: string, private _ref: string, private _nom: string, private _unit: number) {

        /* Construction de la structure HTML */
        this.elem = document.createElement("div");
        this.elem.className = "item bulle";
        this.elem.id = _id;

        this._titre = document.createElement("div");
        this._titre.innerText = _nom;
        this._titre.className = "titre";
        this.elem.appendChild(this._titre);

        this._cont = document.createElement("div");
        this._cont.className = "content";
        this.elem.appendChild(this._cont);

        this._cont_in = document.createElement("div");
        this._cont_in.className = "in";
        this._cont.appendChild(this._cont_in);
        
        this._cont_out = document.createElement("div");
        this._cont_out.className = "out";
        this._cont.appendChild(this._cont_out);

        this._flux_entree = { ...BulleBase.__flux(), existe: true };
        this._flux_entree.box.dataset["flux"] = `${_id}:in:0`;

    }

    _set_flux_out (n: number, connexion?: string) {
        if (n > this._flux_sortie.length) return;
        this._flux_sortie[n].connexion = connexion;
        this._flux_sortie[n].box.classList.toggle("active", !!connexion);        
    }

    _set_flux_in (connexion?: string) {
        this._flux_entree.connexion = connexion;
        this._flux_entree.box.classList.toggle("active", !!connexion);
    }

    /**
     * Liste les flux de sortie de cette bulle
     */
    get fluxSortie (): { id: string, connexion?: string }[] {
        return this._flux_sortie.map((e, k) => ({ 
            id: `${this._id}:out:${k}`, 
            connexion: e.connexion
        }));
    }

    /**
     * Ajoute un argument de sortie à la bulle
     * 
     * @param nom Nom de l'argument
     * @param type Type de l'argument
     * @param defaut Valeur par défaut (au format texte)
     */
    addOutputArg (nom: string, type: Type) {
        let id = `${this._id}:out:${this._arguments.out.length}`;
        let arg = new Argument(id, nom, 'out', type);
        this._arguments.out.push(arg);
        this.render();
    }

    /**
     * Ajoute un argument d'entrée à la bulle
     * 
     * @param nom Nom de l'argument
     * @param type Type de l'argument
     * @param defaut Valeur par défaut (au format texte)
     */
    addInputArg (nom: string, type: Type, defaut: string) {
        let id = `${this._id}:in:${this._arguments.in.length}`;
        let arg = new Argument(id, nom, 'in', type, defaut);
        this._arguments.in.push(arg);
        this.render();
    }

    /**
     * Ajoute un flux de sortie
     * @param nom Nom du flux
     */
    addFlux (nom?: string) {
        let f = BulleBase.__flux(nom);
        let i = this._flux_sortie.push(f);
        f.box.dataset["flux"] = `${this._id}:out:${i - 1}`;
        this.render();
    }

    /**
     * Génère un élément de type `__flux`
     */
    static __flux (nom?: string): __flux {
        
        let elem = document.createElement("div");
        let box = document.createElement("div");
        let txt = document.createElement("div");

        elem.appendChild(box);
        elem.appendChild(txt);

        if (nom) txt.innerText = nom;

        elem.className = "flux"
        txt.className = "txt"
        box.className = "box"

        return {
            box, elem, txt
        }
    }

    /**
     * Actualise le rendu du noeud
     */
    render(unite?: number) {

        let in_height = 0,
            out_height = 0;

        /* On vire tout */
        this._flux_entree.elem.remove();
        this._flux_sortie.forEach(f => f.elem.remove());
        this._arguments.in.forEach(a => a.self.remove());
        this._arguments.out.forEach(a => a.self.remove());

        /* On met à jour */
        if (this._flux_entree.existe) {
            this._cont_in.append(this._flux_entree.elem);
            in_height++;
        }

        for (let f of this._flux_sortie) {
            this._cont_out.append(f.elem);
            out_height++;
        }
            
        for (let a of this._arguments.in) {
            this._cont_in.append(a.self);
            in_height += (a.active && 1) || 2;
        }

        for (let a of this._arguments.out) {
            this._cont_out.append(a.self);
            out_height += 1;
        }

        this._titre.innerText = this._nom;  
        
        /* Mise à jour hauteur */
        this.height = Math.max(in_height, out_height) / 4;

        /* Mise à jour largeur */
        let titre_w = this._titre.getBoundingClientRect().width;
        let in_w = this._cont_in.getBoundingClientRect().width;
        let out_w = this._cont_out.getBoundingClientRect().width;

        this.width = ~~(1.5 + Math.max(titre_w, in_w + out_w) / (unite || this._unit)) / 4;

    }

    /* Exportation de l'élément HTML */
    get self (): HTMLDivElement {
        return this.elem;
    }

    /* Exportation arguments */
    protected get _args (): { in?: __Argument[], out?: __Argument[] } {
        return { 
            in: this._arguments.in.map(e => e.exporte()), 
            out: this._arguments.out.map(e => e.exporte()) 
        };
    };

    protected get _flux (): __Flux {
        return {
            type: this._mode_flux,
            in: [ { nom: undefined, connexion: this._flux_entree.connexion } ],
            out: this._flux_sortie.map(e => ({ nom: e.txt.innerText, connexion: e.connexion })),
        }
    }

    /* Exportation */
    export (): CacheFeuille {
        return {
            type: this._type,
            adresse: this._id,
            ref: this._ref,
            flux: this._flux,
            args: this._args,
            __ui: {
                x: this.x,
                y: this.y,
                h: this.height,
                w: this.width
            }
        }
    }

    get feuille (): __Feuille {
        return this.export();
    }

    /* ID */
    public get id (): string {
        return this._id
    }

    /* X */
    get x (): number {
        return this._x;
    }

    set x (x: number) {
        this._x = x;
        this.elem.style.setProperty("--x", x.toFixed(2));
    }
    
    /* Y */
    get y (): number {
        return this._y;
    }

    set y (y: number) {
        this._y = y;
        this.elem.style.setProperty("--y", y.toFixed(2));
    }

    /* Largeur */
    get width (): number {
        return this._w;
    }

    set width (w: number) {
        this._w = w;
        this.elem.style.setProperty("--w", w.toFixed(2));    
    }

    /* Hauteur */
    get height (): number {
        return this._h;
    }

    set height (h: number) {
        this._h = h;
        this.elem.style.setProperty("--h", h.toFixed(2));    
    }

    /* Couleur 1 */
    get main_color (): string {
        return this._colors[0];
    }
    set main_color (color: string) {
        this._colors[0] = color;
        this.elem.style.setProperty("--col-1", color);
    }

    /* Couleur 2 */
    get sec_color (): string {
        return this._colors[1];
    }
    set sec_color (color: string) {
        this._colors[1] = color;
        this.elem.style.setProperty("--col-2", color);        
    }

    /* Type de bulle */
    get type () {
        return this._type;
    }
    set type (t) {
        this._type = t;
    }

    /* Parent de la bulle */
    get parent () {
        return this._flux_entree.connexion
    }

    /* Suivants */
    get filiation () {
        return this._flux_sortie.map(e => e.connexion);
    }

    /**
     * Donne la position d'un flux sur la grille
     * @param raw Chemin en string
     * @returns la position
     */
    positionFlux (raw: string): { x: number, y: number } | undefined {
        let { id, mode, i } = BulleBase.decomposeId(raw);

        if (id != this._id) return;

        if (mode == "in") {
            if (i > 0) {
                return;
            }
            return { x: this._x * 4 + 0.5, y: this._y * 4 + 0.5 + 1 }
        }

        if (i >= this._flux_sortie.length) {
            return;
        }

        return { x: (this._x + this._w) * 4 - 0.5, y: this._y * 4 + i + 0.5 + 1 }

    }

    /**
     * Donne la position d'un argument sur la grille
     * @param raw Chemin en string
     * @returns la position
     */
    positionArgument (raw: string): { x: number, y: number } | undefined {
        let { id, mode, i } = BulleBase.decomposeId(raw);

        if (id != this._id) return;
        
        if (mode == "in") {
            if (i > this._arguments.in.length) return;
    
            let h = 0;
            for (let j = 0; j < i; j++) h += (this._arguments.in[j].active) && 1 || 2;

            return { x: this._x * 4 + 0.5, y: this._y * 4 + h + 0.5 + 1 + (this._flux_entree.existe && 1 || 0) }
        }

        if (i > this._arguments.out.length) return;

        return { x: (this._x + this._w) * 4 - 0.5, y: this._y * 4 + i + 0.5 + 1 + this._flux_sortie.length }

    }

    /**
     * Connecte un flux 
     * @param raw Chemin en string
     * @param connexion la connexion
     * @returns vrai si la connexion a réussi
     */
    attacheFlux (raw: string, connexion: string): boolean {
        let { id, mode, i } = BulleBase.decomposeId(raw);
        let conn = BulleBase.decomposeId(connexion);

        if (id != this._id) return false;

        if (conn.mode == mode) return false;

        if (mode == "in") {
            
            if (!this._flux_entree.existe) return false;
            if (this._flux_entree.connexion) return false;
            
            this._set_flux_in(connexion);

            return true;

        }

        let flux = this._flux_sortie[i];
        if (!flux) return false;
        if (flux.connexion) return false;

        this._set_flux_out(i, connexion);

        return true;
    }
    
    /**
     * Détache un flux
     * @param raw Chemin du flux
     */
    detacheFlux (raw: string): void {
        let { id, mode, i } = BulleBase.decomposeId(raw);
        if (id != this._id) return;
        if (mode == "in") this._set_flux_in(undefined);
        else this._set_flux_out(i, undefined);
    }

    /**
     * Renvoie à qui est connecté un flux
     * @param raw Chemin du flux
     * @returns La connexion si elle existe
     */
    connexionFlux (raw: string): string | undefined {
        let { id, mode, i } = BulleBase.decomposeId(raw);
        if (id != this._id) return;
        if (mode == 'in') return this._flux_entree.connexion;
        let flux = this._flux_sortie[i];
        if (!flux) return;
        return flux.connexion;
    }

    /**
     * Cache les flux non acceptés
     * @param mode Les flux à ne pas cacher
     */
    cacheFlux (mode: "in" | "out") {

        if (mode == "out") return this._flux_entree.box.classList.add("hidden");

        this._flux_sortie.forEach(f => f.box.classList.add("hidden"));

    }

    /**
     * Affiche les flux cachés
     */
    reveleFlux () {
        this._flux_entree.box.classList.remove("hidden");
        this._flux_sortie.forEach(f => f.box.classList.remove("hidden"));
    }

    /**
     * Cache les arguments non acceptés
     * @param mode Les flux à ne pas cacher
     */
    cacheArgs (mode: "in" | "out") {

        if (mode == "out") return this._arguments.in.forEach(el => el.hidden = true);

        this._arguments.out.forEach(el => el.hidden = true);

    }

    /**
     * Affiche les arguments cachés
     */
    reveleArgs () {
        this._arguments.in.forEach(el => el.hidden = false);
        this._arguments.out.forEach(el => el.hidden = false);
    }

    static decomposeId (rawPath: string): { id: string, mode: string, i: number } {

        let [ a,b,c, mode, n ] = rawPath.split(":");
        return { 
            id: `${a}:${b}:${c}`,
            i: parseInt(n),
            mode
        }

    }

    argument (raw: string): Argument | undefined {

        let { id, mode, i } = BulleBase.decomposeId(raw);
        
        if (id != this._id) return;

        if (mode == "in") return this._arguments.in[i];
        else return this._arguments.out[i];

    }

    /**
     * Supprime le flux en entrée
     */
    rmFluxEntree () {
        this._flux_entree.existe = false;
    }

}