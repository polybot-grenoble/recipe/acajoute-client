import { Type } from "./type";

export class Argument {

    private _active: boolean = false;
    private _hidden: boolean = false;

    protected _elem: HTMLDivElement;
    protected _input: HTMLInputElement;
    
    protected _box: HTMLDivElement;
    protected _txt: HTMLSpanElement;
    protected _defarg: HTMLSpanElement;

    private _connexion: Argument[] = [];

    constructor (
        private _id: string,
        private _nom: string,
        private _mode: 'in' | "out",
        private _type: Type,
        defaut?: string,
    ) {

        this._elem = document.createElement("div");
        this._elem.className = "arg";
        this._elem.id = _id;

        /* Boîte */
        this._box = document.createElement("div");
        this._box.className = "box";
        this._elem.appendChild(this._box);
        this._box.dataset["arg"] = _id;

        /* Texte */
        this._txt = document.createElement("span");
        this._txt.className = "txt";
        this._txt.innerText = _nom;
        this._elem.append(this._txt);        

        /* Valeur par défaut */
        this._defarg = document.createElement("span");
        this._defarg.className = "defarg";

        this._input = document.createElement("input");
        this._input.type = "text";
        if (defaut) 
            this._input.value = defaut;

        this._defarg.append(this._input);
        if (this.mode == "in")
            this._elem.append(this._defarg);

        /* Mise à jour affichage */
        this.type = _type;

    }

    get self (): HTMLDivElement {
        return this._elem;
    }

    get id (): string {
        return this._id;
    }

    get type (): Type {
        return this._type;
    }
    set type (t: Type) {
        this._type = t;
        this._box.style.setProperty("--arg-col", t.couleur);
    }

    get nom (): string {
        return this._nom;
    }
    set nom (nom: string) {
        this._nom = nom;
        this._txt.innerText = nom;
    }

    get active (): boolean {
        return this._active;
    }
    protected set active (a: boolean) {
        this._active = a;
        this._box.classList.toggle("active", a);
    }

    get hidden (): boolean {
        return this._hidden;
    }
    set hidden (h: boolean) {
        this._hidden = h;
        this._box.classList.toggle("hidden", h);
    }

    get mode (): "in" | 'out' {
        return this._mode;
    }

    get connexions (): Argument[] {
        return this._connexion;
    }

    /**
     * Connecte cet argument à un autre si possible
     * 
     * @param arg L'autre argument
     * @returns La connexion a réussi
     */
    connecte (arg: Argument): boolean {
        if (!this.compatible(arg, true)) return false;
        this._connexion.push(arg);
        this.active = true
        return true;
    }

    /**
     * Déconnecte l'argument
     */
    deconnecte (id?: string): void {
        if (id) {
            let tej = this._connexion.filter(e => e.id == id)[0];
            if (tej) tej.deconnecte(this.id); else return;
            this._connexion = this._connexion.filter(e => e.id != id);
            this.active = this._connexion.length != 0;
        } else {
            let temp = [ ...this._connexion ];
            this._connexion = [];
            temp.forEach(e => e.deconnecte(this.id));
            this.active = false;
        }
    }

    /**
     * Vérifie la compatibilité entre cet argument et un autre
     * 
     * @param arg L'argument avec lequel comparer
     * @param exact Vérifie qu'aucun cast n'est necessaire
     * @returns Cet argument est compatible avec l'autre
     */
    compatible (arg: Argument, exact?: boolean): boolean {

        // Si je suis caché, on ne me voit pas
        if (this._hidden) return false;
        
        // Si l'autre est caché, on ne le voit pas
        if (arg.hidden) return false;

        // On ne met pas une entrée sur une entrée ni une sortie sur une sortie
        if (arg.mode == this.mode) return false;

        // On ne passe pas d'un tableau à une variable simple et inversement
        if (arg.type.variante != this.type.variante) return false;
        
        // C'est parfait !
        if (arg.type.id == this.type.id) return true;

        // On ne veut pas de caster si on a "exact"
        if (exact) return false;

        // On cherche s'il existe un caster ?
        let casters = this._type.casts.filter(e => e.mode == this._mode && e.type == arg.type);
        return casters.length > 0;
        
    }

    /**
     * Exporte l'argument
     */
    exporte (): __Argument {
        return {
            _typeObjet: "__Argument",
            nom: this._nom,
            type: this._type.id,
            defaut: this._input.value || undefined,
            precedent: this.mode == "in" ? (this._connexion.length ? this._connexion[0].id : undefined) : undefined
        }
    }

}