export type VarianteType = "simple" | "table" | "objet";

export type Type = {
    id: string,
    nom: string,
    variante: VarianteType,
    couleur: string,
    casts: Caster[]
};

export type Caster = {
    id: string,
    nom: string, 
    type: Type,
    mode: 'in' | 'out',
}

export const INVALID_TYPE: Type = { casts: [], couleur: "var(--bg-4)", id: "__err", nom: "#Erreur", variante: "simple" };