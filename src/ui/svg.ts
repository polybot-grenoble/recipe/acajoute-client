
/*  Le but est de créer une fonction qui retourne un string contenant le code d'une balise <path /> SVG :
    soit une courbe de Bézier, soit un chemin en mode PCB avec 2 points intermédiaires en cas de virage.
    
    La fonction recevra un chemin (départ, arrivée, type de lien, couleur) ainsi que la taille en pixels d'une unité de chemin.
    
    Prototype:*/
    /**
     * Type contenant les données importantes d'un trait : son type, ses coordonnées de départ et d'arrivée et sa couleur
     */
    type coord = { x: number, y: number };
    export type cheminCourbe = { 
        depart: coord, arrive: coord,
        type: 'flux' | 'argument',
        couleur: string
    }
    /**
     * Fonction qui dessine une ligne reliant deux bulles de la page
     * @param chemin paramètres de la courbe, avec notamment son point de départ et celui d'arrivée, son type et sa couleur
     * @param unité taille d'une case du tableau
     * @param distInterLignes distance entre les lignes partant d'une même boîte donnée par le wrapper de la fonction 
     * @param mode_polybot mode qui indique si la ligne doit être droite ou courbée
     * @returns retourne une balise <path /> pour dessiner la ligne dont les paramètres sont passés à la fonction
     */
    export function codeLienBulle (chemin: cheminCourbe, unité: number, distInterLignes: number, mode_polybot?: boolean): string {
        let d : string;
        if (mode_polybot) 
            d = `M ${chemin.depart.x * unité} ${chemin.depart.y * unité} L ${(chemin.depart.x + 0.5 + distInterLignes) * unité} ${chemin.depart.y * unité} L ${(chemin.depart.x + 0.5 + distInterLignes) * unité} ${chemin.arrive.y * unité} L ${chemin.arrive.x * unité} ${chemin.arrive.y * unité}`;
        else
            d = `M ${chemin.depart.x * unité} ${chemin.depart.y * unité} C ${((chemin.arrive.x + chemin.depart.x)/2) * unité} ${chemin.depart.y * unité}, ${((chemin.arrive.x + chemin.depart.x)/2) * unité} ${chemin.arrive.y * unité}, ${chemin.arrive.x * unité} ${chemin.arrive.y * unité}`;
        let autres_params = `stroke-width="${(chemin.type=="flux")?"5":"2"}" stroke="${chemin.couleur}" fill="none" stroke-linecap="round"`;
        return `<path d="${d}" ${autres_params} ></path>`
    }

    /**
     * Fonction qui dessine les lignes qui relient les bulles sur la page
     * @param tabLignes Tableau des paramètres des lignes présentes sur la page à dessiner
     * @param unité taille d'une case du tableau
     * @param mode_polybot mode qui indique si la ligne doit être droite ou courbée
     * @returns retourne toutes les balises <path /> des lignes présente sur la page
     */
    export function codeLienBulleTab(tabLignes: cheminCourbe[], unité: number, mode_polybot?: boolean): string {
        let res = '';
        let compteur = 0;
        let verif = tabLignes[0];
        let lignesEnsembles : cheminCourbe[] = [];
        while(tabLignes.length) {
            while (tabLignes.length && tabLignes[0].depart.x == verif.depart.x){
                let uneLigne = tabLignes[0];
                tabLignes.shift();
                lignesEnsembles.push(uneLigne);
                compteur++;
            }
            let distance = Math.abs(verif.arrive.x - verif.depart.x - 1) / (compteur + 1);
            compteur = 0;
            let multip = 1;
            while(lignesEnsembles.length != 0){
                let uneLigne = lignesEnsembles[0]
                lignesEnsembles.shift()
                res += codeLienBulle(uneLigne, unité, distance * multip, mode_polybot);
                multip++;
            }
            verif = tabLignes[0];
        }
        return res; 
    }