import { Fonction } from "../../fonction";
import { CacheFeuille, CacheFonction, Projet } from "../../projet";
import { Argument } from "../bulles/argument";
import { BulleBase } from "../bulles/bulle";
import { Type } from "../bulles/type";
import { Page, PageCallback, PageManager } from "../pages";
import { cheminCourbe, codeLienBulle, codeLienBulleTab } from "../svg";
import { PageGrille } from "./grille";

export class PageFonction extends PageGrille implements Page {
    
    private _bulles: BulleBase[] = [];
    private _liens: { de: string, a: string, couleur: "flux" | string }[] = [];
    private _svg: SVGSVGElement;

    private _mode_polybot = false;

    protected _etatSouris: {
        mode: 'none';
    } | {
        mode: 'grille'
    } | {
        mode: 'bulle',
        target: BulleBase,
        souris: { x: number, y: number }, // En coordonées unité
        offset: { x: number, y: number }, // En coordonées unité
    } | {
        mode: 'flux' | 'argument',
        origin: string,
        origin_type: 'in' | 'out',
        souris: { x: number, y: number }, // En coordonées unité
        lien: { de: string, a: string, couleur: "flux" | string },
        bulle: BulleBase
    };


    constructor (private _fonction: Fonction, private _projet: Projet, manager: PageManager) {

        super(_fonction.id, _fonction.nom, manager);

        this._page.onclose = m => {
            _fonction.updateCache();
            this.onclose(m);
        }

        this._page.onload = m => {

            this.onload(m);
        }

        /* Init SVG */
        this._svg = <SVGSVGElement>document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this._svg.setAttribute("class", "item");
        this._page.elem.append(this._svg);

        this._restoreRender();

        this._etatSouris = { mode: "none" };

        // console.log(this)

        _fonction.onrequestUpdate = () => this.exporte();
        _fonction.onchange = () => this._restoreRender();

    }
    
    protected onPointerDown (e: PointerEvent): void {

        if (!e.target) return;

        if (e.target == this.elem) {
            // Grille
            this._etatSouris.mode = "grille";
            return this._handleGrilleDown(e);
        }

        if (this.elem.contains(<Node>e.target)) {

            let temp = <HTMLDivElement>e.target;    
            let bulle: BulleBase;

            if (temp.dataset["flux"]) {

                this._handleFluxDown(e);
                
            } else if (temp.dataset["arg"]) {

                this._handleArgDown(e);

            } else if (temp.classList && temp.classList.contains("bulle")) {

                bulle = this.bulle(temp.id);
                // console.log(e);

                this._etatSouris = {
                    mode: "bulle",
                    offset: { 
                        x: ~~(- e.offsetX / this.__grille.unité), 
                        y: ~~(- e.offsetY / this.__grille.unité) 
                    },
                    souris: { x: e.clientX, y: e.clientY },
                    target: bulle,
                }

            }

            // console.log(this._etatSouris, this._etatSouris.mode == "bulle" && this._etatSouris.offset || temp)

        }

    }

    protected onPointerUp (e: PointerEvent): void {
        
        if (this._etatSouris.mode == "grille") {
            // Grille
            this._handleGrilleUp(e);
        } 

        else if (this._etatSouris.mode == "bulle") {
            // Mouvement Bulle
            // Pas de fonction wrapper car pas grand chose à faire
            
            this._etatSouris.target.x = Math.round(4 * this._etatSouris.target.x) / 4;
            this._etatSouris.target.y = Math.round(4 * this._etatSouris.target.y) / 4;

            this.render()
        }

        else if (this._etatSouris.mode == "argument") {
            // Lien entre arguments
            this._handleArgUp(e);
        }

        else if (this._etatSouris.mode == "flux") {
            // Lien entre flux
            this._handleFluxUp(e);
        }
        
        
        this._etatSouris.mode = "none";

    }

    protected onPointerMove (e: PointerEvent): void {
        if (!e.pressure && this._etatSouris.mode != "none" && this._etatSouris.mode != "grille") return this.onPointerUp(e);
        
        if (this._etatSouris.mode == "grille") {
            // Mouvement grille
            this._handleGrilleMove(e);
        }

        else if (this._etatSouris.mode == "bulle") {
            // Mouvement Bulle
            // Pas de fonction wrapper car pas grand chose à faire
            this._etatSouris.souris.x = e.clientX;
            this._etatSouris.souris.y = e.clientY;

            let origine = this._getSelfOrigin();
            let x = (( (this._etatSouris.souris.x - origine.x) / this.__grille.unité) + this._etatSouris.offset.x) / 4,
                y = (( (this._etatSouris.souris.y - origine.y) / this.__grille.unité) + this._etatSouris.offset.y) / 4;

            
            this._etatSouris.target.x = x;
            this._etatSouris.target.y = y;
            this.render()
        }

        else if (this._etatSouris.mode == "argument") {
            // Lien entre arguments
            this._handleArgMove(e);
        }

        else if (this._etatSouris.mode == "flux") {
            // Lien entre flux
            this._handleFluxMove(e);
        }
    
    }

    private _getSelfOrigin (): { x: number, y: number } {
        let bodyBox = document.body.getBoundingClientRect(),
            selfBox = this.elem.getBoundingClientRect();
        return { 
            x: selfBox.left - bodyBox.left - this.__grille.origine.x, 
            y: selfBox.top - bodyBox.top - this.__grille.origine.y
        }
    }

    private _handleFluxDown (e: PointerEvent) {

        let temp = <HTMLDivElement>e.target;
        if (!temp.dataset["flux"]) return;

        let origin = temp.dataset["flux"];
        let chemin = BulleBase.decomposeId(origin);
        let bulle = this.bulle(chemin.id);
        let mode = <'in' | 'out'>chemin.mode;

        // console.log(bulle.positionFlux(origin))

        let connexion = bulle.connexionFlux(origin);

        let lien = { 
            de: "", a: "", couleur: "flux"
        };

        if (chemin.mode == "in") {
            lien.de = "mouse";
            lien.a = origin
        } else {
            lien.a = "mouse";
            lien.de = origin
        }

        if (connexion) {

            lien = this._liens.filter(e => (e.a == origin || e.de == origin) && e.couleur == "flux")[0] || lien;

            if (chemin.mode == "out") {
                lien.de = "mouse";
                lien.a = connexion
            } else {
                lien.a = "mouse";
                lien.de = connexion
            }

            // console.log(lien);

            // On remplace l'origine par le point d'accroche
            let path_co = BulleBase.decomposeId(connexion);
            bulle = this.bulle(path_co.id);
            origin = connexion;
            mode = <'in' | 'out'>path_co.mode;
        }

        this._etatSouris = {
            mode: "flux",
            souris: bulle.positionFlux(origin) || { x: 0, y: 0 },
            origin,
            origin_type: mode,
            lien,
            bulle
        };

        if (!connexion) this._liens.push(this._etatSouris.lien);

        if (mode == "out")
            this._bloqueFluxParent(origin, true, mode);
        else
            this._bloqueFluxFiliation(origin, true, mode);

        this.render();

    }

    private _handleArgDown (e: PointerEvent) {

        let temp = <HTMLDivElement>e.target;
        if (!temp.dataset["arg"]) return;

        let origin = temp.dataset["arg"];
        let chemin = BulleBase.decomposeId(origin);
        let bulle = this.bulle(chemin.id);
        let mode = <'in' | 'out'>chemin.mode;

        let arg = bulle.argument(origin);

        let lien = { 
            de: "", a: "", couleur: bulle.argument(origin)?.type.couleur || 'var(--theme)'
        };

        if (chemin.mode == "in") {
            lien.de = "mouse";
            lien.a = origin
        } else {
            lien.a = "mouse";
            lien.de = origin
        }

        if (arg?.active && mode == "in") {

            let co = <Argument>arg.connexions[0];
            lien = this._liens.filter(e => (e.a == origin || e.de == origin) && e.couleur != "flux")[0] || lien;

            if (chemin.mode == "out") {
                lien.de = "mouse";
                lien.a = co.id
            } else {
                lien.a = "mouse";
                lien.de = co.id
            }

            // On remplace l'origine par le point d'accroche
            let path_co = BulleBase.decomposeId(co.id);
            bulle = this.bulle(path_co.id);
            origin = co.id;
            mode = <'in' | 'out'>path_co.mode;

            arg.deconnecte();
        }

        this._etatSouris = {
            mode: "argument",
            souris: bulle.positionArgument(origin) || { x: 0, y: 0 },
            origin,
            origin_type: mode,
            lien,
            bulle
        };

        this._liens.push(this._etatSouris.lien);

        if (mode == "out")
            this._bloqueArgParent(origin, true, mode);
        else
            this._bloqueArgFiliation(origin, true, mode);

        // console.log(origin, bulle.positionArgument(origin))

        this.render();

    }

    private _detacheFlux (connexion: string) {
        let path = BulleBase.decomposeId(connexion);
        let bulle = this.bulle(path.id);

        bulle.detacheFlux(connexion);
        this._liens = this._liens.filter(e => e.couleur != "flux" || !(e.de == connexion || e.a == connexion));

    }

    private _attacheFlux (de: string, a: string): boolean {

        let path_de = BulleBase.decomposeId(de);
        let path_a = BulleBase.decomposeId(a);

        let bulle_de = this.bulle(path_de.id);
        let bulle_a = this.bulle(path_a.id);

        let c = bulle_de.attacheFlux(de, a);
        let b = bulle_a.attacheFlux(a, de);

        return b && c;
        
    }
    
    private _handleFluxUp (e: PointerEvent) {

        if (this._etatSouris.mode != "flux") return;
        
        // On vérifie où on lâche le traît
        let target = <HTMLDivElement>e.target;

        let prec_conn = this._etatSouris.bulle.connexionFlux(this._etatSouris.origin);
        if (prec_conn) {
            this._detacheFlux(prec_conn);
        }

        this._detacheFlux(this._etatSouris.origin);
        
        if (target.dataset["flux"]){
            
            let f = target.dataset["flux"];
            let path = BulleBase.decomposeId(f)
            let bulle = this.bulle(path.id);

            let autre_flux = bulle.connexionFlux(f);

            if (autre_flux) {
                this._detacheFlux(autre_flux);
                this._detacheFlux(f);
            }

            let suc = this._attacheFlux(this._etatSouris.origin, f);

            if (suc) this._liens.push({
                a: path.mode == "in" ? f : this._etatSouris.origin,
                de: path.mode == "out" ? f : this._etatSouris.origin,
                couleur: 'flux'
            })

        }
        
        else if (target == this.elem) {
            // alert("Menu contextuel tmtc");

            // return; // On ne supprime pas le lien dans ce cas, ce sera géré dans le callback du menu contextuel
        }

        // Suppression du lien vers la souris
        let l = this._etatSouris.lien;
        this._liens = this._liens.filter(e => e != l);

        this._bloqueFluxParent(this._etatSouris.origin, false, this._etatSouris.origin_type);
        this._bloqueFluxFiliation(this._etatSouris.origin, false, this._etatSouris.origin_type);

        this.render();

        this._fonction.updateCache();

    }
    
    private _handleArgUp (e: PointerEvent) {

        if (this._etatSouris.mode != "argument") return;
        
        // On vérifie où on lâche le traît
        let target = <HTMLDivElement>e.target;
        
        let origin = this._etatSouris.bulle;
        let { mode } = BulleBase.decomposeId(this._etatSouris.origin);

        let arg = origin.argument(this._etatSouris.origin);

        if (target.dataset["arg"]){
            // On ajoute cette connexion sans décrocher les anciennes 
            // pour l'argument de sortie
            
            let a = target.dataset["arg"];
            let path = BulleBase.decomposeId(a)
            let bulle = this.bulle(path.id);
            let autre_arg = bulle.argument(a);

            if (path.mode == mode) return; // Pas compatible

            if (mode == "in") {

                if (arg?.active) {
                    arg.deconnecte()
                }

            } else {

                if (autre_arg?.active) {
                    autre_arg.deconnecte();
                }

            }
        
            this._connecteArguments(this._etatSouris.origin, a);

            this._fonction.updateCache();

        }
        
        else if (target == this.elem) {

            // return; // On ne supprime pas le lien dans ce cas, ce sera géré dans le callback du menu contextuel
        }

        // Suppression du lien vers la souris
        let l = this._etatSouris.lien;
        this._liens = this._liens.filter(e => e != l);

        this._bloqueArgParent(this._etatSouris.origin, false, this._etatSouris.origin_type);
        this._bloqueArgFiliation(this._etatSouris.origin, false, this._etatSouris.origin_type);

        this.render();

    }

    private _handleFluxMove (e: PointerEvent) {

        if (this._etatSouris.mode != "flux") return;

        let x = e.clientX;
        let y = e.clientY;

        let origine = this._getSelfOrigin();

        x -= origine.x;
        y -= origine.y;

        x /= this.__grille.unité;
        y /= this.__grille.unité;

        this._etatSouris.souris.x = x;
        this._etatSouris.souris.y = y;

        this.render()

    }

    private _bloqueFluxParent (connexion: string, bloque: boolean, mode: "in" | "out") {
        let { id } = BulleBase.decomposeId(connexion);
        let bulle = this.bulle(id);

        if (!bloque) {
            bulle.reveleFlux();
        } else {
            bulle.cacheFlux(mode);
        }

        if (bulle.type == "racine") return;

        if (bulle.parent) this._bloqueFluxParent(bulle.parent, bloque, mode);

    }

    private _bloqueArgParent (connexion: string, bloque: boolean, mode: "in" | "out") {
        let { id } = BulleBase.decomposeId(connexion);
        let bulle = this.bulle(id);

        if (!bloque) {
            bulle.reveleArgs();
        } else {
            bulle.cacheArgs(mode);
        }

        if (bulle.type == "racine") return;

        if (bulle.parent) this._bloqueArgParent(bulle.parent, bloque, mode);

    }

    private _handleArgMove (e: PointerEvent) {

        if (this._etatSouris.mode != "argument") return;

        let x = e.clientX;
        let y = e.clientY;

        let origine = this._getSelfOrigin();

        x -= origine.x;
        y -= origine.y;

        x /= this.__grille.unité;
        y /= this.__grille.unité;

        this._etatSouris.souris.x = x;
        this._etatSouris.souris.y = y;

        this.render()

    }


    private _bloqueFluxFiliation (connexion: string, bloque: boolean, mode: "in" | "out") {
        let { id } = BulleBase.decomposeId(connexion);
        let bulle = this.bulle(id);

        if (!bloque) {
            bulle.reveleFlux();
        } else {
            bulle.cacheFlux(mode);
        }

        if (bulle.type == "retour") return;

        for (let f of bulle.filiation) {
            if (!f) continue;
            this._bloqueFluxParent(f, bloque, mode)
        }

    }

    private _bloqueArgFiliation (connexion: string, bloque: boolean, mode: "in" | "out") {
        let { id } = BulleBase.decomposeId(connexion);
        let bulle = this.bulle(id);

        if (!bloque) {
            bulle.reveleArgs();
        } else {
            bulle.cacheArgs(mode);
        }

        if (bulle.type == "retour") return;

        for (let f of bulle.filiation) {
            if (!f) continue;
            this._bloqueArgParent(f, bloque, mode)
        }

    }
        
    render (): void {
        this.updateGrille();
        this.renderLiens();
    }

    get racine (): BulleBase {
        return this.bulle(this._fonction.racine);
    }

    bulle (id: string): BulleBase {
        return this._bulles.filter(e => e.id == id)[0];
    }

    get mode_polybot () {
        return this._mode_polybot
    }

    set mode_polybot (m: boolean) {
        this._mode_polybot = m;
        this.render();
    }

    /**
     * Fait le rendu des liens
     */
    renderLiens (): void {

        // Mieux vaudrait un tableau de path
        let rendu: string = "";
        
        let souris = this._liens.filter(l => l.a == "mouse" || l.de == "mouse")[0];
        let liste = this._liens.filter(l => l.a != "mouse" && l.de != "mouse" && l.couleur == "flux");
        let args = this._liens.filter(l => l.a != "mouse" && l.de != "mouse" && l.couleur != "flux");

        let toRender = [
            ...liste.map(e => lien_vers_chemin_flux(e, this._bulles)), // Flux
            ...args.map(e => lien_vers_chemin_args(e, this._bulles)), // Args
        ]

        rendu += codeLienBulleTab(toRender, this.__grille.unité, this._mode_polybot);
        

        if (souris) {
            rendu += this._renderLienSouris(souris);
        }

        this._svg.innerHTML = rendu;

    }
    
    /**
     * Calcule un lien entre un bloc et la souris 
     * */
    private _renderLienSouris (l: {de: string, a: string, couleur: "flux" | string}): string {
        if (this._etatSouris.mode != "flux" && this._etatSouris.mode != "argument") return "";

        let map_de = this._etatSouris.mode == "argument" 
            ? (b: BulleBase) => b.positionArgument(l.de)
            : (b: BulleBase) => b.positionFlux(l.de);

        let map_a = this._etatSouris.mode == "argument" 
            ? (b: BulleBase) => b.positionArgument(l.a)
            : (b: BulleBase) => b.positionFlux(l.a);

        let de = this._bulles.map(map_de).filter(pos => pos)[0] || this._etatSouris.souris;
        let a = this._bulles.map(map_a).filter(pos => pos)[0] || this._etatSouris.souris;

        let c: cheminCourbe = {
            arrive: a, couleur: l.couleur == "flux" ? "var(--fg-1)" : l.couleur, depart: de, type: this._etatSouris.mode
        }

        return codeLienBulle(c, this.__grille.unité, 0, this._mode_polybot);
    }

    /**
     * Fait le rendu à partir du cache
     */
    private _restoreRender () {

        // RàZ des états
        this._bulles.forEach(b => b.self.remove());
        this._bulles = [];
        this._liens = [];

        this._restoreBulles();
        this._restoreArguments();
        this._restoreLiens();

    }

    /**
     *  Re-calcule les liens entre bulles 
     */
    private _restoreLiens () {

        let _parcours: BulleBase[] = [];
        let _a_explorer: BulleBase[] = [ this.racine ];

        let _in = (tab: BulleBase[], el: BulleBase) => tab.indexOf(el) != -1;
        
        while (_a_explorer.length) {
    
            let b = _a_explorer.shift();
            if (!b) break;  // Sécurité
            
            _parcours.push(b);
            
            let flux = b.fluxSortie;

            for (let f of flux) {
                if (f.connexion) {
                    let p = BulleBase.decomposeId(f.connexion);
                    
                    let autre = this.bulle(p.id);

                    if (!_in(_parcours, autre))
                        _a_explorer.push(autre);
                    
                    this._liens.push({ de: f.id, a: f.connexion, couleur: "flux" })
                }
            }

        }

        this.renderLiens();

    }

    /** 
     * Restaure les bulles 
     * */
    private _restoreBulles (): void {
        
        this._bulles = [];

        for (let _f in this._fonction.feuilles) {
            let F = this._fonction.feuilles[_f];

            // Purement temporaire: ne prend pas en compte les types de bulles !!!!
            let ref = this._projet.ref(F.ref);
            let bulle = new BulleBase(_f, F.ref, ref ? ref.nom : F.ref, this.__grille.unité);
            if (ref) {
                bulle.main_color = ref.couleur;
                if (!ref.flux_entree) bulle.rmFluxEntree();
            }

            bulle.x = F.__ui.x;
            bulle.y = F.__ui.y;
            bulle.width = F.__ui.w;
            bulle.height = F.__ui.h;
            bulle.type = F.type;

            if (F.flux) {

                if (F.flux.in.length)
                    bulle._set_flux_in(F.flux.in[0].connexion);

                if (F.flux.out.length)
                    for (let _f = 0; _f < F.flux.out.length; _f++) {
                        bulle.addFlux(F.flux.out[_f].nom);
                        bulle._set_flux_out(_f, F.flux.out[_f].connexion);
                    }

            }

            if (F.args) {

                let _t: Type;

                if (F.args.in) {
                    for (let arg of F.args.in) {
                        _t = this._projet.type(arg.type);
                        bulle.addInputArg(arg.nom, _t, arg.defaut || '');
                    }
                }

                if (F.args.out) {
                    for (let arg of F.args.out) {
                        _t = this._projet.type(arg.type);
                        bulle.addOutputArg(arg.nom, _t);
                    }
                }

            }

            this._bulles.push(bulle);
            this._page.elem.append(bulle.self);
            bulle.render();
            
        }

    }

    /**
     * Restore les liasons entre arguments
     */
    private _restoreArguments () {

        for (let _f in this._fonction.feuilles) {

            let feuille = this._fonction.feuilles[_f];

            if (feuille.args?.in) {
                for (let i = 0; i < feuille.args.in.length; i++) {
                    let arg = feuille.args.in[i];
                    if (!arg.precedent) continue;
                    this._connecteArguments(arg.precedent, `${_f}:in:${i}`);                    
                }
            }

        }

    }

    /**
     * Retourne un argument selon le chemin indiqué
     * 
     * @param raw Chemin vers un argument
     * @returns l'argument s'il existe
     */
    private _getArgument (raw: string): Argument | undefined {

        let { id } = BulleBase.decomposeId(raw);
        let bulle = this.bulle(id);

        return bulle.argument(raw);

    }

    /**
     * Essaie de connecter deux arguments 
     * @param de Chemin du premier argument
     * @param a Chemin du second argument
     * @returns la connexion a réussi
     */
    private _connecteArguments (de: string, a: string): boolean {

        let arg_de = this._getArgument(de);
        let arg_a = this._getArgument(a);

        if (!arg_de || !arg_a) return false;

        let suc = arg_de.connecte(arg_a);
        if (!suc) return false;

        suc = arg_a.connecte(arg_de);
        if (!suc) return false;

        this._liens.push({ de, a, couleur: arg_a.type.couleur })

        this.render()

        return true;

    }

    /**
     * Exporte la fonction décrite visuellement dans la page
     * @returns Le cache de la fonction
     */
    exporte (): CacheFonction {

        let bulleDico: { [key: string]: CacheFeuille } = {};

        for (let b of this._bulles) {
            bulleDico[b.id] = b.export();
        }

        return {
            adresse: this._fonction.id,
            feuilles: bulleDico,
            nom: this._fonction.nom,
            inputArgs: this._fonction.inputArgs,
            outputArgs: this._fonction.outputArgs,
            racine: this._fonction.racine
        }

    }

}

const lien_vers_chemin_flux: (l: { de: string, a: string, couleur: 'flux' | string }, bulles: BulleBase[]) => cheminCourbe = 
    (l, bulles) => {
        let de = bulles.map(b => b.positionFlux(l.de)).filter(pos => pos)[0]  || { x: 0, y: 0 };
        let a = bulles.map(b => b.positionFlux(l.a)).filter(pos => pos)[0]  || { x: 0, y: 0 };

        return { 
            arrive: a,
            depart: de,
            couleur: "var(--fg-1)",
            type:"flux",
        }
    };

const lien_vers_chemin_args: (l: { de: string, a: string, couleur: string }, bulles: BulleBase[]) => cheminCourbe = 
    (l, bulles) => {
        let de = bulles.map(b => b.positionArgument(l.de)).filter(pos => pos)[0]  || { x: 0, y: 0 };
        let a = bulles.map(b => b.positionArgument(l.a)).filter(pos => pos)[0]  || { x: 0, y: 0 };
        // console.log(l)
        return { 
            arrive: a,
            depart: de,
            couleur: l.couleur,
            type:"argument",
        }
    };
