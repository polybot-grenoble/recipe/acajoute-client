import { Page, PageCallback, PageManager } from "../pages";

export class PageGrille implements Page {

    protected __grille: {

        unité: number, 
        origine: { x: number, y: number },

        __etat: {
            touchId: number[],
            touchEvents: PointerEvent[],
            preventMovement: boolean,

            pinchDistance: number,
        },

    };

    protected _page: Page;
    
    public onload: PageCallback = () => {};
    public onclose: PageCallback = () => {};

    constructor (readonly id: string, readonly nom: string, manager: PageManager) {

        this._page = <Page>manager.addPage(`grille_${id}`, nom);

        this.__grille = {

            unité: 24,
            origine: { x: 0, y: 0 },
    
            __etat: {
                touchId: [],
                touchEvents: [],
                preventMovement: false,
    
                pinchDistance: 0,
            }
    
        };

        
        let grille = this._page.elem;
        grille.classList.add("grille");

        grille.onpointerdown = e => this.onPointerDown(e);
        
        grille.onpointerup = e => this.onPointerUp(e);
        grille.onpointerleave = e => this._handleGrilleUp(e);
        
        grille.onpointermove = e => this.onPointerMove(e);

        grille.onwheel = e => this.onWheel(e);

    }

    render (): void {
        this.updateGrille();
    }

    /**
     * Est déclenché lors de l'activation d'un pointeur
     * @param page La page
     * @param e L'évènement pointeur
     */
    protected onPointerDown (e: PointerEvent): void {

        this._handleGrilleDown(e);

    }

    protected _handleGrilleDown (e: PointerEvent) {
        this.__grille.__etat.touchId.push(e.pointerId);
        this.__grille.__etat.touchEvents[e.pointerId] = e;

        if (this.__grille.__etat.touchId.length == 2) {
            let ids = this.__grille.__etat.touchId;
            let events = ids.map(n => this.__grille.__etat.touchEvents[n]);
            this.__grille.__etat.pinchDistance = pinchDistance(events[0], events[1]);
        }
    }

    /**
     * Est déclenché lors du relâchement d'un pointeur
     * @param page La page
     * @param e L'évènement pointeur
     */
    protected onPointerUp (e: PointerEvent): void {
        
        this._handleGrilleUp(e);

    }

    protected _handleGrilleUp (e: PointerEvent) {

        this.__grille.__etat.touchId = this.__grille.__etat.touchId.filter(i => i != e.pointerId);
        delete this.__grille.__etat.touchEvents[e.pointerId];

    }

    get elem () {
        return this._page.elem;
    }

    get onglet () {
        return this._page.onglet;
    }
    
    /**
     * Est déclenché lors du mouvement d'un pointeur
     * @param page La page
     * @param e L'évènement pointeur
     */
    protected onPointerMove (e: PointerEvent): void {
    
        this._handleGrilleMove(e);
    
    }

    protected _handleGrilleMove (e: PointerEvent) {
        // On vérifie que le pointeur est actif
        if (this.__grille.__etat.touchId.indexOf(e.pointerId) == -1) return;
        let old_events = [ ...this.__grille.__etat.touchEvents ];
        this.__grille.__etat.touchEvents[e.pointerId] = e;
    
        // Si il n'y a pas de pression, on regarde pas
        if (this.__grille.__etat.touchId.length == 0) return;
    
        // Si il y a un curseur, c'est un déplacement
        if (this.__grille.__etat.touchId.length == 1 && !this.__grille.__etat.preventMovement) {
            let id = this.__grille.__etat.touchId[0];
            let event = this.__grille.__etat.touchEvents[id];
            let old = old_events[id];
    
            let mx = ( old.clientX - event.clientX );
            let my = ( old.clientY - event.clientY );
    
            this.moveGrille(mx, my);
    
        }
    
        // Si il y a deux curseurs, c'est un zoom
        if (this.__grille.__etat.touchId.length == 2 && !this.__grille.__etat.preventMovement) {
            let ids = this.__grille.__etat.touchId;
            let events = ids.map(n => this.__grille.__etat.touchEvents[n]);
    
            let dist = pinchDistance(events[0], events[1]);
            let dDist = ((dist - this.__grille.__etat.pinchDistance) / 10); 
            this.__grille.__etat.pinchDistance = dist;
    
            let centre = {
                x: (events[0].offsetX + events[1].offsetX) / 2,
                y: (events[0].offsetY + events[1].offsetY) / 2,
            };
            this.zoomGrille(dDist, centre);
    
        }
    
    }

    /**
     * Déclenché lors de l'utilisation de la molette
     * @param page La page
     * @param e L'évènement molette
     */
    protected onWheel (e: WheelEvent): void {
    
        e.preventDefault();
        let centre = {
            x: e.offsetX, y: e.offsetY
        }
        this.zoomGrille(-e.deltaY / 50, centre);
    
    }

    /**
     * Déplace l'origine de la grille
     * @param page La page
     * @param dx Le déplacement sur X
     * @param dy Le déplacement sur Y
     */
    protected moveGrille (dx: number, dy: number): void {
    
        this.__grille.origine.x += dx;
        this.__grille.origine.y += dy;
        this.render();
    
    }

    /**
     * Modifie le zoom sur la grille d'une page
     * @param page La page
     * @param dzoom La différence de zoom
     * @param centre le "centre" du zoom
     */
    protected zoomGrille (dzoom: number, centre: { x: number, y: number }): void {
    
        dzoom += 4 * ~~(dzoom / 4);
    
        // On retiens l'ancienne unité
        let _unité_bak = this.__grille.unité;
    
        // On calcule la nouvelle unité
        let unité = this.__grille.unité = Math.max(12, Math.min(52, this.__grille.unité + dzoom));
    
        // Je me souviens plus pourquoi ça marche, mais ça permet de déplacer le "centre" du zoom
        this.__grille.origine.x = ((unité * (this.__grille.origine.x + centre.x)) / _unité_bak) - centre.x;
        this.__grille.origine.y = ((unité * (this.__grille.origine.y + centre.y)) / _unité_bak) - centre.y;
    
        // On met à jour l'affichage
        this.render();
    
    }
    
    /**
     * Met à jour la grille d'une page
     * @param page La page
     */
    protected updateGrille (): void {

        this.elem.style.setProperty("--gx", `${this.__grille.origine.x}`);
        this.elem.style.setProperty("--gy", `${this.__grille.origine.y}`);
        this.elem.style.setProperty("--unit", `${this.__grille.unité}px`);

    }

}


/**
 * Calcule la distance entre deux doigts sur un écran tactile
 * @param ev0 Le premier doigt
 * @param ev1 Le second doigt
 * @returns la distance entre les deux doigts
 */
function pinchDistance (ev0: PointerEvent, ev1: PointerEvent): number {
    let dx = ev0.clientX - ev1.clientX;
    let dy = ev0.clientY - ev1.clientY;
    return Math.sqrt( (dx ** 2) + (dy ** 2) );
}
