import { Projet } from "../../projet";
import { HTMLOnglet } from "../onglets";
import { Page, PageCallback, PageManager } from "../pages";

export class PageProjet implements Page {

    public onload: PageCallback = () => {};
    public onclose: PageCallback = () => {};
    
    private _page: Page;

    constructor (private _projet: Projet, manager: PageManager) {

        // A changer:
        this._page = manager.addPage(this.id, this.nom);

        this._page.onclose = m => {

            this.onclose(m);
        }

        this._page.onload = m => {

            this.onload(m);
        }

        this._page.elem.append(this.nom);

    }

    get id (): string {
        return this._projet.id;
    }

    get nom (): string {
        return this._projet.nom;
    }

    get elem (): HTMLDivElement {
        return this._page.elem;
    }

    get onglet (): HTMLOnglet {
        return this._page.onglet;
    }    

} 