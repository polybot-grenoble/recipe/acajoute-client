/**
 * Les onglets on été écrits dans un style fonctionnel malgré quelques callbacks
 */

export type HTMLOnglet = HTMLDivElement & { dataset: { id: string } };

export type BarreOnglets = {
    barre: HTMLDivElement,
    onglets: HTMLOnglet[],
    actif: number,
    __dragndrop: {
        drag: true,
        elem: HTMLOnglet,
        absPos: { x: number, y: number },
        offsetPos: { x: number, y: number },
        pointerId: number,
        posTable: number[],
        elemIndex: number,
    } | { 
        drag: false 
        dragTimeout: number,
    },
    onchange: (nom: string) => void,
    onclose: (nom: string) => void,
}

/**
 * Fonction permettant d'initialiser une barre d'onglets
 * @param barre Element DIV contenant les futurs onglets
 * @returns L'objet représentant la barre d'onglets
 */
export function initBarreOnglets (barre: HTMLDivElement): BarreOnglets {

    let onglets: BarreOnglets = {
        barre, onglets: [], actif: 0,
        __dragndrop: { drag: false, dragTimeout: -12 },
        onchange: () => {},
        onclose: () => {},
    };

    document.addEventListener("pointermove", e => onPointerMoveGlobal(e, onglets));

    return onglets;

}

/**
 * Fonction ajoutant un onglet
 * @param onglets L'objet représentant la barre d'onglets
 * @param nom Le nom de l'onglet
 * @returns L'onglet
 */
export function addOnglet (onglets: BarreOnglets, nom: string, id: string): HTMLOnglet {
    let elem: HTMLOnglet = <HTMLOnglet>document.createElement("div");

    elem.innerHTML = `<div class="wrap"> <label>${nom}</label> <i class="fa fa-solid fa-times"></i> </div>`;
    elem.dataset.id = id;
    // elem.draggable = true;
    elem.className = "onglet";

    onglets.onglets.push(elem);

    // Mise en place des évènements Drag'n'drop
    elem.onpointermove = e => onPointerMove(e, onglets, elem);
    // elem.onpointerleave = e => onPointerMoveEnd(e, onglets, elem);

    let croix = <HTMLSpanElement>elem.querySelector("i");

    // Mise en place de l'évènement "Fermeture d'onglet"
    croix.onclick = () => onClickCroix(onglets, elem);

    renderBarre(onglets);

    return elem;

}

/**
 * Fonction indiquant l'onglet sélectionné
 * @param onglets L'objet représentant la barre d'onglets
 * @returns Le nom de l'onglet sélectionné ou `""` si invalide
 */
export function getOngletValue (onglets: BarreOnglets): string {
    let onglet = onglets.onglets[onglets.actif];
    if (!onglet) return "";
    return onglet.dataset.id;
}

/**
 * Fonction permettant de sélectionner l'onglet actif
 * @param onglets L'objet représentant la barre d'onglets
 * @param id L'id de l'onglet à sélectionner
 */
export function selectOnglet (onglets: BarreOnglets, id: string): void {
    for (let i = 0; i < onglets.onglets.length; i++) {
        if (onglets.onglets[i].dataset.id == id) {
            onglets.actif = i;
            renderBarre(onglets);
            onglets.onchange(getOngletValue(onglets));
            break;
        }
    }
}

/**
 * Fonction mettant à jour le rendu
 * @param onglets L'objet représentant la barre d'onglets
 */
function renderBarre (onglets: BarreOnglets) {
    // On enlève tout le temps de réorganiser
    onglets.onglets.forEach(e => e.remove());
    
    // On récupère tout le monde Sauf l'élément transporté s'il existe
    let renderList = onglets.onglets.filter(e => !onglets.__dragndrop.drag || e != onglets.__dragndrop.elem);
  
    // Si on transporte quelqu'un, on doit trouver sa place
    if (onglets.__dragndrop.drag) {
        renderList.splice(onglets.__dragndrop.elemIndex, 0, onglets.__dragndrop.elem);
        onglets.actif = onglets.__dragndrop.elemIndex;
    }

    // On sauvegarde la nouvelle liste des onglets
    onglets.onglets = renderList;

    // On affiche tout le monde
    renderList.forEach((e, i) => {
        onglets.barre.appendChild(e)
        
        // Si c'est l'actif, on ajoute une class CSS pou le visualiser
        e.classList.toggle("actif", i == onglets.actif);
        
        // On met à jour l'évènemnt click car les indices ont changé
        e.onclick = () => {
            if (onglets.__dragndrop.drag) return;

            // On définit l'actif comme l'indice actuel
            onglets.actif = i;

            // On prévient du changement
            onglets.onchange(getOngletValue(onglets));
            
            // On re-fait le rendu
            renderBarre(onglets);
        }
    });

}

/**
 * Fonction déclenchée en début de mouvement
 * @param e Evènement pointerMove sur l'onglet
 * @param onglets L'objet représentant la barre d'onglets
 * @param onglet L'onglet
 */
function onPointerMove (e: PointerEvent, onglets: BarreOnglets, onglet: HTMLOnglet): void {

    e.preventDefault();

    if (e.pressure) {
        // @ts-ignore
        if (!onglets.__dragndrop.drag && onglets.__dragndrop.dragTimeout == -12) {
            console.log("Init timeout")
            // @ts-ignore
            onglets.__dragndrop.dragTimeout = setTimeout(() => {
                onPointerMoveStart(e, onglets, onglet);
            }, 200);
        }

        
        // On ajoute un évènement sur la disparition du pointeur
        let listen = () => {

            if (onglets.__dragndrop.drag) {
                return;
            }

            // Si il y a un timeout et qu'on a relâché la pression => on vire le timeout
            // @ts-ignore
            if (onglets.__dragndrop.dragTimeout != -12) {
                // @ts-ignore
                clearTimeout(onglets.__dragndrop.dragTimeout);
                // @ts-ignore
                onglets.__dragndrop.dragTimeout = -12;
            } 
            
        };
        document.body.addEventListener("pointerleave", listen, { once: true });

        return;
    }

}

function onPointerMoveGlobal (e: PointerEvent, onglets: BarreOnglets) {
    
    if (onglets.__dragndrop.drag) {
        // On utilise qu'un seul pointeur
        if (e.pointerId != onglets.__dragndrop.pointerId) return;

        if (!e.pressure) {
            // On a lâché l'onglet
            onPointerMoveEnd(e, onglets, onglets.__dragndrop.elem);
            return;
        }

        // On met à jour l'affichage de l'onglet transporté
        onglets.__dragndrop.absPos.x = e.clientX;
        onglets.__dragndrop.absPos.y = e.clientY;
        renderDrag(onglets);

        // Met à jour le rendu
        renderBarre(onglets);

        return;
    } 

    // Si il y a un timeout et qu'on a relâché la pression => on vire le timeout
    // @ts-ignore
    if (onglets.__dragndrop.dragTimeout != -12 && !e.pressure) {
        // @ts-ignore
        clearTimeout(onglets.__dragndrop.dragTimeout);
        // @ts-ignore
        onglets.__dragndrop.dragTimeout = -12;
    } 

}

/**
 * Fonction appelée lorsque l'onglet est attrapé
 * @param e Evènement pointerMove sur l'onglet
 * @param onglets L'objet représentant la barre d'onglets
 * @param onglet L'onglet
 */
function onPointerMoveStart (e: PointerEvent, onglets: BarreOnglets, onglet: HTMLOnglet): void {

    // On lève le drapeau
    onglets.__dragndrop.drag = true;
    if (!onglets.__dragndrop.drag) return;
    
    // On indique quel est le pointeur utilisé
    onglets.__dragndrop.pointerId = e.pointerId;
    
    // On récupère les dimensions de l'objet
    let { width, height } = onglet.getBoundingClientRect();
    
    // On définit cet élément comme celui déplacé
    onglets.__dragndrop.elem = onglet;

    // On définit le décalage avec lequel la souris à capturé l'onglet
    onglets.__dragndrop.offsetPos = {
        x: width / 2,// e.offsetX,
        y: height / 2//e.offsetY
    };
    
    // On définit la position de la souris
    onglets.__dragndrop.absPos = {
        x: e.clientX, 
        y: e.clientY
    };

    // On définit cet élément comme actif
    onglets.actif = onglets.onglets.indexOf(onglet);

    // On prévient du changement d'actif
    onglets.onchange(getOngletValue(onglets));
 
    // On met à jour la liste des tailles d'onglets
    onglets.__dragndrop.posTable = onglets.onglets.filter(e => e != onglet).map(o => {
        let rect = o.getBoundingClientRect();
        return rect.x + rect.width / 2;
    });

    // On ajoute la class "drag" de l'onglet
    onglet.classList.add("drag");
    renderDrag(onglets);
   
    // Met à jour le rendu
    renderBarre(onglets);
    onglets.barre.style.setProperty("--drag-w", `${width}px`);

    // On ajoute un évènement sur la disparition du pointeur
    let listen = e => {

        onPointerMoveEnd(e, onglets, onglet);

        if (!onglets.__dragndrop.drag) {
            document.body.removeEventListener("pointerleave", listen);
            return;
        }
        
        if (e.pointerId != onglets.__dragndrop.pointerId) return;

        document.body.removeEventListener("pointerleave", listen);
        
    };
    document.body.addEventListener("pointerleave", listen);

}

/**
 * Fonction appelée lorsque l'onglet est lâché
 * @param e Evènement pointerMove sur l'onglet
 * @param onglets L'objet représentant la barre d'onglets
 * @param onglet L'onglet
 */
function onPointerMoveEnd (e: PointerEvent, onglets: BarreOnglets, onglet: HTMLOnglet): void {

    // On baisse le drapeau
    onglets.__dragndrop.drag = false;
    if (!onglets.__dragndrop.drag) {
        // @ts-ignore
        onglets.__dragndrop.dragTimeout = -12;
    }   
    
    // On enlève la class "drag" de l'onglet
    onglet.classList.remove("drag");

    // On enlève le CSS
    onglet.setAttribute("style", "");
    
    // On enlève toutes les class "drag" au cas où
    onglets.onglets.forEach(onglet => onglet.classList.remove("drag"));
    
}

/**
 * Met à jour l'onglet déplacé
 * @param onglets L'objet représentant la barre d'onglets 
 */
function renderDrag (onglets: BarreOnglets): void {

    if (!onglets.__dragndrop.drag) return;

    let x = onglets.__dragndrop.absPos.x;
    let y = onglets.__dragndrop.absPos.y;

    onglets.__dragndrop.elem.style.left = `${ x - onglets.__dragndrop.offsetPos.x }px`;
    onglets.__dragndrop.elem.style.top = `${ y - onglets.__dragndrop.offsetPos.y }px`;

    let i: number;
    let table = onglets.__dragndrop.posTable;
    for (i = 0; i < table.length; i++) {
        if (x < table[i]) break;
    }
    onglets.__dragndrop.elemIndex = i;

}

/**
 * Fonction déclenchée lors du click sur la croix
 * @param onglets L'objet représentant la barre d'onglets
 * @param onglet L'onglet
 */
function onClickCroix (onglets: BarreOnglets, onglet: HTMLOnglet) {
    
    closeOnglet(onglets, onglet);
    
    // On prévient la fermeture de cet onglet
    onglets.onclose(onglet.dataset.id);

}

/**
 * Fonction permettant de fermer un onglet
 * @param onglets L'objet représentant la barre d'onglets
 * @param onglet L'onglet
 */
export function closeOnglet (onglets: BarreOnglets, onglet: HTMLOnglet) {

    // On désactive le click
    onglet.onclick = () => {};

    let idx = onglets.onglets.indexOf(onglet);

    // Si l'onglet n'est déjà plus là, on a plus rien à faire
    if (idx == -1) return;
    
    // On vérifie si cet onglet est actif
    if (idx == onglets.actif) {
        // Si cet élément est le dernier, alors actif devient l'avant-dernier
        if (idx == onglets.onglets.length - 1) 
            onglets.actif = idx - 1;
        
    }

    // Si cet élément est avant l'actif on doit décrémenter actif
    else if (idx < onglets.actif) 
        onglets.actif--;
        
    // On supprime l'onglet de la liste des onglets
    onglets.onglets = onglets.onglets.filter(e => e != onglet);
    onglet.remove();
    renderBarre(onglets);

    // On prévient que l'onglet actif a changé
    onglets.onchange(getOngletValue(onglets));    

}