export type BoiteBulleType = "simple" | "objet" | 'tableau';

const nom_icone = {
    "simple": "project-diagram",
    "objet": "tools",
    "tableau": "table",
}

export class boiteBulle {
    _type : string;
    _nom : string;
    _couleur : string;
    _li : HTMLLIElement;
    _i : HTMLElement;
    _span : HTMLSpanElement;

    constructor(type: BoiteBulleType, nom, couleur) {
        this._type = type;
        this._nom = nom;
        this._couleur = couleur;
        this._li = document.createElement("li");
        this._li.className = "expand_list_elem boite_a_bulle";
        let div = document.createElement("div");
        div.className = "selector";
        this._i = document.createElement("i");
        this._i.className = "fa-solid fa-" + nom_icone[this._type] + " fa";
        this._span = document.createElement("span");
        this._span.className = "text";
        this._span.innerText = this._nom;
        div.append(this._i);
        this._li.append(div, this._span);
        this._li.style.setProperty("--couleur_passage", couleur);
    }

    get type() {
        return this._type;
    }

    get nom() {
        return this._nom;
    }

    get couleur() {
        return this._couleur;
    }

    get self(){
        return this._li;
    }

    set nom(nouv_nom){
        this._nom = nouv_nom;
        this._span.innerText = this._nom;
    }

    set type(nouv_type){
        this._type = nouv_type;
        this._i.className = "fa-solid fa-" + nom_icone[this._type] + " fa";
    }

    set couleur(nouv_couleur){
        this._couleur = nouv_couleur;
        this._li.style.setProperty("--couleur_passage", nouv_couleur);
    }
}