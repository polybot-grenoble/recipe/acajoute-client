import { BoiteBulleType, boiteBulle } from "./boitebulle";



function genBoite (id: string, nom: string, logo: string): string {

    return`
<input class="__menu_select" type="checkbox" id="${id}">
<label class="titre" for="${id}">
    <div class="selector">
        <i class="fa-solid fa-${logo} fa"></i>
    </div>
    <span class="text">${nom}</span>
    <i class="fa-solid fa-plus fa"></i>
    <i class="fa-solid fa-minus fa"></i>
</label>
`
}

export class grosseBoite {
    _num_menu : number;
    _nom : string;
    _logo : string;
    _div : HTMLDivElement;
    _ul : HTMLUListElement;
    _input: HTMLInputElement;

    tabBulle : boiteBulle[] = [];
    
    constructor(num_menu, nom, logo){
        this._num_menu = num_menu;
        this._nom = nom;
        this._logo = logo;
        this._div = document.createElement("div");
        this._div.className = "expand";
        this._div.style.setProperty("--nb-elem", "0");
        this._ul = document.createElement("ul");
        this._ul.className = "expand_list";
        this._div.innerHTML = genBoite("grosse_boite_" + num_menu.toString(), this._nom, this._logo);
        this._div.append(this._ul);
        this._input = <HTMLInputElement>this._div.querySelector("input");
    }

    get self(){
        return this._div;
    }

    creerBulle(type: BoiteBulleType, nom_bulle, couleur){
        const bulle = new boiteBulle(type, nom_bulle, couleur);
        this._ul.append(bulle.self);
        this.tabBulle.push(bulle);
        this._div.style.setProperty("--nb-elem", this.tabBulle.length.toString());
        return bulle;
    }

    clear () {
        this.tabBulle.forEach(e => e.self.remove());
        this.tabBulle = [];
    }

    get opened () {
        return this._input.checked;
    }
    set opened (o: boolean) {
        this._input.checked = o;
    }

}